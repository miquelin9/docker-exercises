#EJERCICIO 4

FROM nginx:1.19.3-alpine\
\
RUN apk --no-cache add curl\
\
HEALTHCHECK --interval=45s --timeout=5s --start-period=30s --retries=2 CMD curl --fail http://localhost:80
