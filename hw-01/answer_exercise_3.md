#EJERCICIO 3
\
	1- docker pull nginx:1.19.3-alpine\
	2- docker volume create static_content\
	3- docker run -p --name custom_container -v static_content:/usr/share/nginx/html/ -d nginx:1.19.3-alpine\
	4- docker exec -it custom_container bin/bash\
	5- cd /usr/share/nginx/html/\
	6- cat > index.html\
	7- escribir "HOMEWORK 1" + [Ctrl][C]
