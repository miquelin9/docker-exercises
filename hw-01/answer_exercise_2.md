#EJERCICIO 2
\
Aunque ADD y COPY son prácticamente lo mismo, es preferible usar COPY.\
Esto, es debido a que es mas transparente que ADD.\
COPY solo soporta la copia de ficheros locales a un contenedor,\
mientras que ADD soporta soporte para URL remotas.
